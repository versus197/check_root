package tk.chefbook.checkroot;

import com.androidquery.AQuery;
import com.stericson.RootTools.RootTools;

import android.os.Bundle;
import android.provider.Settings;
import android.provider.Settings.SettingNotFoundException;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.pm.ActivityInfo;
import android.util.Log;
import android.view.View;

public class MainActivity extends Activity {

	private AQuery aq;
	private ProgressDialog pd;
	private boolean isNonPlayAppAllowed;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation (ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		setContentView(R.layout.activity_main);
		aq = new AQuery(this);
		aq.id(R.id.b_Exit).clicked(this, "doExit");
		aq.id(R.id.b_Check).clicked(this, "doCheckRoot");
		doCheckRoot();
		
	}
	
	public void doExit(View button){
		Log.d("VERSUS","Do exit start");
		finish();
	}
	
	@SuppressWarnings("deprecation")
	public void doCheckRoot(){
		  pd = new ProgressDialog(this);
	      pd.setTitle("Check root");
	      pd.setMessage("One moment");
	      pd.show();
			try {
				isNonPlayAppAllowed = Settings.Secure.getInt(getContentResolver(), 
					Settings.Secure.INSTALL_NON_MARKET_APPS) == 1;
			} catch (SettingNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    		if (isNonPlayAppAllowed) {
    			aq.id(R.id.textUnknownSources).text(R.string.yes);
    		}
	      new Thread(new Task()).start();
	}
	
    class Task implements Runnable {
        @Override
        public void run() {
                try {
                	Log.d("VERSUS","Do check root start");
            		if (RootTools.isRootAvailable()) {
            			aq.id(R.id.textSu).text(R.string.yes);
            		}
            		if (RootTools.isAccessGiven()) {
            			aq.id(R.id.textRoot).text(R.string.yes);
            		}
            		if(RootTools.isBusyboxAvailable()){
            			aq.id(R.id.textBusyBox).text(R.string.yes);
            		}						

                    Thread.sleep(100);
                	pd.dismiss();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                
            }
        }
 
	
}
